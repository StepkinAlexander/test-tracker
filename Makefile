all:
	echo 'nop'

install:
	virtualenv python
	python/bin/pip install -r requirements.txt

run:
	python/bin/python manage.py runserver
