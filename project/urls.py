from django.conf.urls import url
from django.contrib import admin
from django.views.generic import TemplateView

from apps.tracker import views as tracker_views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', tracker_views.home, name='home'),
    url(r'^tasks/$', tracker_views.get_tasks, name='my_tasks'),
    url(r'^task/edit/(?P<task_id>\d+)/$', tracker_views.edit_task, name='edit_task'),
    url(r'^other/$', TemplateView.as_view(template_name='other.html')),
    url(r'^report_list/(\d{1,2}-\d{1,2}-\d{4})/$', tracker_views.get_resolved_tasks_by_day, name='report_resolved'),
    url(r'^report_list/(\d+)/(\d{1,2}-\d{1,2}-\d{4})/$', tracker_views.get_resolved_user_tasks_by_day, name='user_report_resolved'),
    url(r'^ureport_list/(\d+)/$', tracker_views.get_unresolved_user_tasks, name='user_report_unresolved'),
    url(r'^count/$', tracker_views.get_count_resolved_tasks_by_day, name='count'),
]
