# coding: utf-8
from __future__ import unicode_literals

from django.db import models


class Specialization(models.Model):
    name = models.CharField(u'специализация', max_length=255)
    '''сопутствующая информация и навыки'''


class Employee(models.Model):
    fio = models.CharField(u'фио', max_length=255)
    spec = models.ForeignKey(Specialization)
    level = models.IntegerField(default=1)


class Task(models.Model):
    name = models.CharField(u'Краткое описание задачи', max_length=255)
    descr = models.TextField(u'Описание задачи', blank=True, null=True)
    spec = models.ForeignKey(Specialization)
    estimate = models.IntegerField(u'оценено по времени')


class TaskTrack(models.Model):
    '''здесь отмечаем время исполнения задачи'''
    employee = models.ForeignKey(Employee)
    task = models.ForeignKey(Task)
    spending = models.IntegerField(u'потрачено времени в часах', default=0)
    date = models.DateTimeField(u'дата работы', auto_now_add=True)
    done = models.BooleanField(u'задача завершена?', default=False)


class DayTasks(models.Model):
    '''здесь задачи на день'''
    task = models.ForeignKey(Task)
    employee = models.ForeignKey(Employee)
    '''порядок выполнения задач, форма должна следить, чтобы предыдущие(меньший ордер) задачи были отмечены'''
    order = models.IntegerField(u'порядок задач')
    spending = models.IntegerField(u'потрачено времени в часах', default=0)
    done = models.BooleanField(u'задача завершена?', default=False)
