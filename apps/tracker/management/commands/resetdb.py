# coding: utf-8
import random
from subprocess import call
from django.core.management.base import BaseCommand

from apps.tracker.models import Specialization, Employee, Task, TaskTrack, DayTasks

def db_reset():
    call('python manage.py sqlflush | python manage.py dbshell', shell=True)
    print 'База очищена'

SPEC_LIST = ['разработка', 'управление']
def create_spec(name):
    speciality = Specialization(name=name)
    speciality.save()

EMPL_LIST = [
    dict(
        fio='Иванов И.И.',
        spec=SPEC_LIST[0],
        level=5
    ),
    dict(
        fio='Иванов И.П.',
        spec=SPEC_LIST[0],
        level=3
    ),
    dict(
        fio='Иванов И.В.',
        spec=SPEC_LIST[0],
        level=1
    ),
    dict(
        fio='Босс',
        spec=SPEC_LIST[1],
        level=10
    ),
]
def get_specialization(spec_name):
    return Specialization.objects.get(name=spec_name)

def create_employee(fio, spec, level):
    speciality = get_specialization(spec)
    employee = Employee()
    employee.fio = fio
    employee.spec = speciality
    employee.level = level
    employee.save()

TASKS_COUNT = 100
def create_task(name, descr, spec, estimate):
    task = Task.objects.create(name=name, descr=descr, spec=spec, estimate=estimate)
    task.save()


class Command(BaseCommand):

    def handle(self, *args, **options):
        db_reset()
        call('python manage.py migrate', shell=True)

        for spec in SPEC_LIST:
            create_spec(spec)

        for empl in EMPL_LIST:
            create_employee(empl.get('fio'), empl.get('spec'), empl.get('level'))


        for i in range(TASKS_COUNT):
            name = 'task %s' % (i + 1)
            descr = 'original description of task %s' % (i + 1)
            spec = get_specialization(SPEC_LIST[random.randint(0,1)])
            estimate = random.randint(1,6)
            create_task(name=name, descr=descr, spec=spec, estimate=estimate)

        print 'Записано задач: %s' %len(Task.objects.all())
