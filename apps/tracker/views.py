# coding: utf-8
import random
import pytz
from datetime import datetime

from django.contrib import messages
from django.shortcuts import render, redirect
from django.http import JsonResponse

from .models import (
        Employee, Task, TaskTrack, DayTasks
    )
from .forms import DayTasksForm

def home(request):
    return render(request, 'home.html', {
        'today': datetime.now(),
    })


def get_employee(user):
    '''функция отдающая работника совершившего запрос, реализацию тут опускаю'''
    return Employee.objects.all()[0]

def get_my_today_tasks(employee):
    return DayTasks.objects.filter(employee=employee).order_by('order')

def generate_today_tasks(employee):
    tasks = Task.objects.filter(spec=employee.spec)
    hours = 0
    daily = []
    order = 0
    while hours < 8:
        task = tasks[random.randint(0,len(tasks)-1)]
        if task not in daily:
            order += 1
            DayTasks.objects.create(employee=employee, task=task, order=order)
            TaskTrack.objects.create(employee=employee, task=task)
            daily.append(task)
            hours += task.estimate
    return get_my_today_tasks(employee)

def get_tasks(request):
    '''список моих задач'''
    employee = get_employee(request.user)
    my_tasks = get_my_today_tasks(employee)
    if not my_tasks:
        my_tasks = generate_today_tasks(employee)

    return render(request, 'tasks_list.html', {
        'tasks': my_tasks,
    })

def edit_task(request, task_id):
    task = DayTasks.objects.get(id=task_id)
    if request.method == 'POST':
        form = DayTasksForm(request.POST or None, request.FILES or None, instance=task)
        if form.is_valid():
            employee = get_employee(request.user)

            # проверять что предыдущие задачи исполнены, иначе ошибка
            tasks_before = (DayTasks.objects
                .filter(employee=employee, order__lt=task.order)
                .filter(spending=0))
            if tasks_before:
                messages.error(request, u'Не сделаны предыдущие задачи! Не сохранено')
                return redirect('/tasks/')

            done = request.POST.get('done')
            spend = request.POST.get('spend')

            # сохранить изменения в daytasks
            '''здесь можно сделать покрасивее, но на скорую руку так'''
            now = datetime.now().replace(tzinfo=pytz.UTC)
            track_task = (TaskTrack.objects.get(
                            employee=employee, task=task.task,
                            date__gt=now.replace(hour=0, minute=0, second=0, microsecond=0),
                            date__lt=now.replace(hour=23, minute=59, second=59, microsecond=999)))
            if done == 'on' and int(spend)>0:
                task.done = True
                track_task.done = True
            elif done == 'on':
                messages.error(request, u'Нельзя закрыть задачу не поработав над ней')
                return redirect('/tasks/')
            else:
                task.done = False
                track_task.done = False
            task.spending = spend
            task.save()

            # отразить прогресс в tasktrack
            track_task.spending = spend
            track_task.save()

            # отразить в task
            '''
            в данном примере не будет реализовано
            '''

            return redirect('/tasks/')
        else:
            messages.error(request, u'В форме ошибки')
    return render(request, 'task_edit.html', {
        'task': task,
    })

'''
запросы для задачи 4.2
'''

def get_resolved_tasks_by_day(request, date):
    dt = datetime.strptime(date, '%d-%m-%Y').replace(tzinfo=pytz.UTC)
    tasks = TaskTrack.objects.filter(
        date__gt=dt.replace(hour=0, minute=0, second=0, microsecond=0),
        date__lt=dt.replace(hour=23, minute=59, second=59, microsecond=999),
        done=True)
    return render(request, 'resolved_by_day.html', {
        'date': dt,
        'tasks': tasks,
    })

def get_resolved_user_tasks_by_day(request, employee_id, date):
    employee = None
    try:
        int(employee_id)
    except:
        messages.error(request, u'Ошибка параметра, ожидался запрос вида /report_list/int/dd-mm-yyyy')
        return redirect('/')
    else:
        employee = get_employee(employee_id)
    dt = datetime.strptime(date, '%d-%m-%Y').replace(tzinfo=pytz.UTC)
    tasks = TaskTrack.objects.filter(
        employee=employee,
        date__gt=dt.replace(hour=0, minute=0, second=0, microsecond=0),
        date__lt=dt.replace(hour=23, minute=59, second=59, microsecond=999),
        done=True)
    return render(request, 'resolved_by_day.html', {
        'date': dt,
        'employee': employee,
        'tasks': tasks,
    })

def get_count_resolved_tasks_by_day(request):
    dt = datetime.now().replace(tzinfo=pytz.UTC)
    tasks = TaskTrack.objects.filter(
        date__gt=dt.replace(hour=0, minute=0, second=0, microsecond=0),
        date__lt=dt.replace(hour=23, minute=59, second=59, microsecond=999),
        done=True)
    return JsonResponse({'count':len(tasks)})

def get_unresolved_user_tasks(request, employee_id):
    employee = None
    try:
        int(employee_id)
    except:
        messages.error(request, u'Ошибка параметра, ожидался запрос вида /report_list/int')
        return redirect('/')
    else:
        employee = get_employee(employee_id)
    tasks = TaskTrack.objects.filter(
        employee=employee,
        done=False)
    return render(request, 'unresolved_user_tasks.html', {
        'employee': employee,
        'tasks': tasks,
    })
