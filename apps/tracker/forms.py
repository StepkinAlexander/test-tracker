# coding: utf-8
from django import forms
from django.core.exceptions import ValidationError
from django.http import HttpResponseBadRequest

from .models import DayTasks


class DayTasksForm(forms.ModelForm):
    spend = forms.IntegerField()

    class Meta:
        model = DayTasks
        fields = ('done', )
        exclude=('employee', 'task', )
        labels = {
            'spend': u'Затрачено времени'
        }

    def clean(self):
        try:
            int(self.cleaned_data.get('spend'))
        except:
            raise ValidationError(u'Ошибка в данных. Ожидалось число')

        if int(self.cleaned_data.get('spend')) < 0:
            raise ValidationError(u'Ошибка в данных. Ожидалось положительное число')

        return self.cleaned_data
